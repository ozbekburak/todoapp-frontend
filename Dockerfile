FROM node:lts-alpine as builder

# install simple http server for serving static content
RUN npm install -g http-server

# make the 'app' folder the current working directory
WORKDIR /usr/src/app

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# install project dependencies
RUN npm --verbose install

# build app for production with minification
RUN npm run build

FROM nginx:1.18
WORKDIR /usr/share/nginx/html
COPY --from=builder /usr/src/app/dist .
EXPOSE 80



