import { mount, shallowMount } from '@vue/test-utils';
import List from '@/components/List.vue';

describe('List component unit Tests: ', () => {

    it('renders todo table', () => {
        const wrapper = shallowMount(List, {
            stubs: ['b-table']
        })
        expect(wrapper.html()).toContain('b-table')
    });

    it('is data shown in table', async () => {
        const wrapper = mount(List, {
            stubs: ['b-table']
        })

        await wrapper.setData(
            {
                todoList: [{
                    "id": 1,
                    "todo": "buy some milk"
                }]
            }
        )

        expect(wrapper.find('.error').exists()).toBe(false)
    })

    it('table is empty, must show error', async () => {
        const wrapper = mount(List, {
            stubs: ['b-table']
        })

        await wrapper.setData(
            {
                todoList: "no rows found!"
            }
        )

        expect(wrapper.find('.error').exists()).toBe(true)
    })
})