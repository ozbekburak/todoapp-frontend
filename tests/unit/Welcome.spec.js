import { mount, shallowMount } from '@vue/test-utils';
import Welcome from '@/components/Welcome.vue';

describe('Welcome component unit Tests: ', () => {
    it('snapshot', () => {
        const wrapper = mount(Welcome)
        expect(wrapper).toMatchSnapshot()
    })

    it('is a vue instance', () => {
        const wrapper = shallowMount(Welcome, {
            propsData: {
                msg: "Welcome to Your Todo App"
            }
        });
        expect(wrapper.text()).toBeTruthy();
    })
})