import { mount, shallowMount } from '@vue/test-utils';
import Add from '@/components/Add.vue';

describe('Add component unit Tests: ', () => {

    it('snapshot', () => {
        const wrapper = mount(Add)
        expect(wrapper).toMatchSnapshot()
    })

    it('calls addTodo when the add button is clicked', () => {
        const wrapper = shallowMount(Add)
        
        const addTodo = jest.spyOn(wrapper.vm, 'addTodo')
        wrapper.find('button').trigger('submit');

        expect(addTodo).toHaveBeenCalled();
    });

    it('renders user input area', () => {
        const wrapper = shallowMount(Add)
        expect(wrapper.html()).toContain('todo-input')
    });

    it('check valid input', async () => {
        const wrapper = mount(Add)

        await wrapper.setData(
            {
                todo: 'write acceptance test'
            }
        )

        expect(wrapper.find('.error').exists()).toBe(false)
    })

    it('check empty input', async () => {
        const wrapper = mount(Add)
    
        await wrapper.setData(
            {
                todo: ''
            }
        )
    
        expect(wrapper.find('.error').exists()).toBe(true)
    })

    
})
